package com.myawesomecoin.leocoin.leocoin.block;

import com.myawesomecoin.leocoin.leocoin.Transaction.Transaction;
import com.myawesomecoin.leocoin.leocoin.security.ShaUtil;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by amitosh  on 17/02/18.
 */
public class Ablock {
    public String hash;
    public String previousHash;
    public String merkleRoot;
    public ArrayList<Transaction> transactions = new ArrayList<Transaction>();
    public long timeStamp;
    public int nonce;


    public Ablock(String previousHash ) {
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();

        this.hash = calculateHash();
    }
    public String calculateHash() {
        String calculatedhash = ShaUtil.applySha256(
                previousHash +
                        Long.toString(timeStamp) +
                        Integer.toString(nonce) +
                        merkleRoot
        );
        return calculatedhash;
    }
    public boolean addTransaction(Transaction transaction) {
        //process transaction and check if valid, unless block is genesis block then ignore.
        if(transaction == null) return false;
        if((previousHash != "0")) {
            if((transaction.processTransaction() != true)) {
                System.out.println("Transaction failed to process. Discarded.");
                return false;
            }
        }
        transactions.add(transaction);
        System.out.println("Transaction Successfully added to Block");
        return true;
    }

}
