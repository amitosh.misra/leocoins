package com.myawesomecoin.leocoin.leocoin.Transaction;

/**
 * Created by amitosh  on 17/02/18.
 */
public class TransactionInput {
    public String transactionOutputId;
    public TransactionOutput UTXO;

    public TransactionInput(String transactionOutputId) {
        this.transactionOutputId = transactionOutputId;
    }
}
