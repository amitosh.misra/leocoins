package com.myawesomecoin.leocoin.leocoin.Transaction;

import com.myawesomecoin.leocoin.leocoin.LeocoinApplication;
import com.myawesomecoin.leocoin.leocoin.security.ShaUtil;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;


/**
 * Created by amitosh  on 17/02/18.
 */
public class Transaction {

    public String transactionId;
    public PublicKey sender;
    public PublicKey reciepient;
    public float value;
    public byte[] signature;

    public ArrayList<TransactionInput> inputs = new ArrayList<TransactionInput>();
    public ArrayList<TransactionOutput> outputs = new ArrayList<TransactionOutput>();

    private static int sequence = 0; // a rough count of how many transactions have been generated.


    public Transaction(PublicKey from, PublicKey to, float value,  ArrayList<TransactionInput> inputs) {
        this.sender = from;
        this.reciepient = to;
        this.value = value;
        this.inputs = inputs;
    }



    // This Calculates the transaction hash (which will be used as its Id)
    private String calulateHash() {
        sequence++; //increase the sequence to avoid 2 identical transactions having the same hash
        return ShaUtil.applySha256(
                ShaUtil.getStringFromKey(sender) +
                        ShaUtil.getStringFromKey(reciepient) +
                        Float.toString(value) + sequence
        );
    }
    //Signs all the data we dont wish to be tampered with.
    public void  generateSignature(PrivateKey privateKey) {
        String data = ShaUtil.getStringFromKey(sender) + ShaUtil.getStringFromKey(reciepient) + Float.toString(value)	;
        signature = ShaUtil.applyECDSASig(privateKey,data);
    }
    //Verifies the data we signed hasnt been tampered with
    public boolean verifiySignature() {
        String data = ShaUtil.getStringFromKey(sender) + ShaUtil.getStringFromKey(reciepient) + Float.toString(value)	;
        return ShaUtil.verifyECDSASig(sender, data, signature);
    }
    //Returns true if new transaction could be created.
    public boolean processTransaction() {

        if(verifiySignature() == false) {
            System.out.println("#Transaction Signature failed to verify");
            return false;
        }

        //gather transaction inputs (Make sure they are unspent):
        for(TransactionInput i : inputs) {
            i.UTXO = LeocoinApplication.UTXOs.get(i.transactionOutputId);
        }

        //check if transaction is valid:
        if(getInputsValue() < LeocoinApplication.minimumTransaction) {
            System.out.println("#Transaction Inputs to small: " + getInputsValue());
            return false;
        }

        //generate transaction outputs:
        float leftOver = getInputsValue() - value; //get value of inputs then the left over change:
        transactionId = calulateHash();
        outputs.add(new TransactionOutput( this.reciepient, value,transactionId)); //send value to recipient
        outputs.add(new TransactionOutput( this.sender, leftOver,transactionId)); //send the left over 'change' back to sender

        //add outputs to Unspent list
        for(TransactionOutput o : outputs) {
            LeocoinApplication.UTXOs.put(o.id , o);
        }

        //remove transaction inputs from UTXO lists as spent:
        for(TransactionInput i : inputs) {
            if(i.UTXO == null) continue; //if Transaction can't be found skip it
            LeocoinApplication.UTXOs.remove(i.UTXO.id);
        }

        return true;
    }

    //returns sum of inputs(UTXOs) values
    public float getInputsValue() {
        float total = 0;
        for(TransactionInput i : inputs) {
            if(i.UTXO == null) continue; //if Transaction can't be found skip it
            total += i.UTXO.value;
        }
        return total;
    }

    //returns sum of outputs:
    public float getOutputsValue() {
        float total = 0;
        for(TransactionOutput o : outputs) {
            total += o.value;
        }
        return total;
    }
}
