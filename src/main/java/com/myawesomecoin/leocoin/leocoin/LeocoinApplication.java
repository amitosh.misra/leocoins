package com.myawesomecoin.leocoin.leocoin;

import com.myawesomecoin.leocoin.leocoin.Transaction.Transaction;
import com.myawesomecoin.leocoin.leocoin.Transaction.TransactionOutput;
import com.myawesomecoin.leocoin.leocoin.block.Ablock;
import com.myawesomecoin.leocoin.leocoin.miner.MineBlock;
import com.myawesomecoin.leocoin.leocoin.security.ChainValidation;
import com.myawesomecoin.leocoin.leocoin.wallet.Wallet;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;

//@SpringBootApplication
public class LeocoinApplication {

	public static ArrayList<Ablock> blockchain = new ArrayList<Ablock>();
	public static HashMap<String,TransactionOutput> UTXOs = new HashMap<String,TransactionOutput>();
	public static int difficulty = 0;
	public static float minimumTransaction = 0.1f;
	public static Wallet walletA;
	public static Wallet walletB;
	public static Transaction genesisTransaction;

	public static void main(String[] args) {
		//SpringApplication.run(LeocoinApplication.class, args);

		BouncyCastleProvider BouncyCastleProvider = new BouncyCastleProvider();
		Security.addProvider(BouncyCastleProvider);

		walletA = new Wallet();
		walletB = new Wallet();
		Wallet coinbase = new Wallet();


		genesisTransaction = new Transaction(coinbase.publicKey, walletA.publicKey, 100f, null);

		//manually sign the genesis transaction
		genesisTransaction.generateSignature(coinbase.privateKey);

		//manually set the transaction id
		genesisTransaction.transactionId = "0";

		//manually add the Transactions Output
		genesisTransaction.outputs.add(new TransactionOutput(genesisTransaction.reciepient,
				genesisTransaction.value, genesisTransaction.transactionId));

        //store first transaction
		UTXOs.put(genesisTransaction.outputs.get(0).id, genesisTransaction.outputs.get(0));

		System.out.println("Creating and Mining Genesis block... ");
		Ablock genesis = new Ablock("0");
		genesis.addTransaction(genesisTransaction);
		addBlock(genesis);

		//Add Block
		Ablock block1 = new Ablock(genesis.hash);
		System.out.println("\nWalletA's balance is: " + walletA.getBalance());
		System.out.println("\nWalletA is Attempting to send funds (40) to WalletB...");
		block1.addTransaction(walletA.sendFunds(walletB.publicKey, 40f));
		addBlock(block1);
		System.out.println("\nWalletA's balance is: " + walletA.getBalance());
		System.out.println("WalletB's balance is: " + walletB.getBalance());

		Ablock block2 = new Ablock(block1.hash);
		System.out.println("\nWalletA Attempting to send more funds (1000) than it has...");
		block2.addTransaction(walletA.sendFunds(walletB.publicKey, 1000f));
		addBlock(block2);
		System.out.println("\nWalletA's balance is: " + walletA.getBalance());
		System.out.println("WalletB's balance is: " + walletB.getBalance());

		Ablock block3 = new Ablock(block2.hash);
		System.out.println("\nWalletB is Attempting to send funds (20) to WalletA...");
		block3.addTransaction(walletB.sendFunds( walletA.publicKey, 20));
		System.out.println("\nWalletA's balance is: " + walletA.getBalance());
		System.out.println("WalletB's balance is: " + walletB.getBalance());

		ChainValidation.isChainValid(blockchain,genesisTransaction,difficulty);
	}
	public static void addBlock(Ablock newBlock ) {
		new MineBlock().mineBlock(difficulty,newBlock);
		blockchain.add(newBlock);
	}
}
