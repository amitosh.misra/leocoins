package com.myawesomecoin.leocoin.leocoin.miner;


import com.myawesomecoin.leocoin.leocoin.block.Ablock;
import com.myawesomecoin.leocoin.leocoin.security.ShaUtil;

/**
 * Created by amitosh  on 16/02/18.
 */
public class MineBlock {

    public void mineBlock(int difficulty, Ablock block) {
        block.merkleRoot = ShaUtil.getMerkleRoot(block.transactions);
        String target = new String(new char[difficulty]).replace('\0', '0'); //Create a string with difficulty * "0"
        while(!block.hash.substring( 0, difficulty).equals(target)) {
            block.nonce ++;
            block.hash = block.calculateHash();
        }
        System.out.println("Block Mined!!! : " + block.hash);
    }
}
